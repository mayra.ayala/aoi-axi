//Backend
//***************************************************** Setup de eventos a escuchar
require('events').EventEmitter.defaultMaxListeners = 20
//***************************************************** HTTPS server setup
//-----* Express inicia servidor / carpeta raiz
//--------------------------------------------------------------Express inicia servidor -------------------------------------------//
const express = require('express')
const app = express()
const fs = require('fs')
//const ImageDataURI = require('image-data-uri')
app.use(express.static(__dirname))//Carpeta de donde sirve / carpeta raiz public

const server = app.listen(8888, () => {
    console.log('Servidor web iniciado')
})

//-----* Filesystem module object
var fss = require('fs')
//-----* https module object
var https = require('https')

var io = require('socket.io')(server)


//-------------------------------------------------------------------- Postgres module object -----------------------------------------//
const { Pool } = require('pg')
pool= new Pool({
    host: 'localhost',
    user: 'axoi',
    password: 'axoi',
    database: 'golden',
})
let client

//-------------------------------------------------------------- Sockets front ------------------------------------------------//
io.on('connection', (socket) => {

    socket.on('openclient', async function () { // conexion con main_script
        await openconnection()
        console.log("Client on...")
     });
 socket.on('closeclient', async function () { // conexion con main_script
       await closeconnection()
     });
    socket.on('experiment', async function () { // conexion con main_script
        await experiment()
        console.log("this is experiment... ")
     })
     socket.on('insert', async function (contenido,idprograma,componente,defecto,fecha) { // conexion con main_script
        await insert(contenido,idprograma,componente,defecto,fecha)
        console.log(contenido,idprograma,componente,defecto,fecha)
        console.log("funcion insert in backend...")
     })
     socket.on('readfiles', async function () { // conexion con main_script
        await readfile()
        console.log("read files...")
     })
     socket.on('exist', async function (estacionAOI,elementoarray1,elementoarray2) { // conexion con main_script
       console.log("Recibe backend: ",estacionAOI)
       console.log("Recibe backend: ",elementoarray1)
       console.log("Recibe backend: ",elementoarray2)
        await existe(estacionAOI,elementoarray1,elementoarray2)
     })
     socket.on('AlldataBD', async function () {
        await AlldataBD()
        console.log("Extrayendo datos de BD")
     })


})//close io.on

//---------------------------------------------------------------- Funciones para abrir conexion con el cliente pg ---------------------------------------------------------//
async function openconnection(){
    client = await pool.connect()
}
async function closeconnection(){
    return new Promise(async resolve =>{
    client.end()
    client.release()
    resolve('resolved')})}

//Funcion imprime cuantos clientes estan conectados en la base de datos 
async function experiment(){
    console.log("total", pool.totalCount)
    console.log("idle",pool.idleCount)
    console.log("clientes esperando", pool.waitingCount)}
//---------------------------------- QUERYS ---------------------------------------------------------------//
//-----------------------------------Funcion inserta  -----------------------------------------------------//
async function insert(contenido,idprograma,componente,defecto,fecha){ 
    // return new Promise(async resolve => {
         console.log("entre a insertar")
         console.log("estoy en funcion insertar",contenido,idprograma,componente,defecto,fecha)
         let pg="INSERT INTO unidades VALUES ('" + contenido + "','"+ idprograma+"','"+componente +"', '"+ defecto+"','"+ fecha +"')"

         client.query(pg, (err, result) => {
             if (err) {
                 return console.error('Error executing query', err.stack)
               }
               console.log(result.rows)})}

async function existe(estacion,componente,defecto){
    return new Promise(async resolve => {
        console.log("Entre a consulta exist :) ")
        
        //let pg = "SELECT COUNT (*) FROM unidades WHERE estacion = ('"+estacion+"')"
        let pugy= "SELECT EXISTS (SELECT 1 FROM unidades WHERE estacion = ('"+estacion+"') AND componente = ('"+componente+"') AND defecto = ('"+defecto+"') )";
        //let pg = "SELECT estacion,componente,defecto FROM unidades WHERE EXISTS(SELECT * FROM unidades WHERE estacion = ('"+estacion+"') AND componente = ('"+componente+"') AND defecto = ('"+defecto+"') )"
        //let pg = "SELECT * FROM unidades WHERE EXISTS (SELECT 1 FROM unidades WHERE estacion = ('"+estacion+"') AND componente = ('"+componente+"') AND defecto = ('"+defecto+"') )"
          console.log("Query: "+pugy)
          client
           .query(pugy)
            .then((result) => {io.emit('bdresponse', {result,estacion, componente,defecto});console.log(result)}) // brianc
           .catch((err) => console.error('Error executing query', err.stack))
           resolve('resolved');
            })
        }

            
//---------------------------------- Leer logs de falla -------------------------------------------------//
async function readfile(){
const archivo = 'C:/Users/mayra_ayala/Documents/projecto-javascript/archivosread/[FAIL][2023-10-4 2_11_44]283005040267.txt'
fs.readFile(archivo, 'utf-8', (err, data) => {
    if (err){
       console.error('error al leer el archivo: ', err)
       return
    }
    console.log('Contenido del archivo: '+'\n' + data)
  
    io.emit('datatext',data)
})
}

async function AlldataBD(){
    return new Promise(async resolve => {
        let pgy = "SELECT * FROM unidades"

        console.log("Query: "+pgy)
          client
        .query(pgy)
        .then((result) => {io.emit('dataBD', result);console.log(result)}) // brianc
        .catch((err) => console.error('Error executing query', err.stack))
        resolve('resolved');
    })
}



