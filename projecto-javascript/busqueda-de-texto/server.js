
const fs = require('fs')
//Especificar la ruta en donde estara alojado el archivo txt 
const archivo = 'C:/Users/mayra_ayala/Documents/projecto-javascript/busqueda-de-texto/archivosvarios/miarchivo.txt'

//leer un archivo 
fs.readFile(archivo, 'utf-8', (err, data) => {
 if (err){
    console.error('error al leer el archivo: ', err)
    return
 }
 console.log('Contenido del archivo: '+ '\n ' + data)


//Practicas con los datos 
//Imprime en consola numero de lineas encontradas en el texto 
const lineas = data.split('\n')
console.log('Numero de lineas: ',lineas.length)

//Imprime en consola el numero de palabras encontradas en el texto 
//Nota* Solo si estan en la primera linea
const palabras = data.split(' ')
console.log('Numero de palabras: ', palabras.length)


//Buscar una palabra en especifico 
let palabra = 'C45'
 if (data.indexOf(palabra) !== -1){
    console.log('Encontre esa palabra en el archivo', palabra)
    }else{
        console.log('No encontre esa palabra', data)
    }

// Buscar numeros dentro del archivo txt 
const numerosEncontrados = data.match(/\d+/g)

if(numerosEncontrados){
    console.log('Numeros encontrados', numerosEncontrados)
}else{
    console.log('No se encontraron numeros :(')
}

})

// Buscar un archivo txt en una ruta externa 
/*
const url = 'https://ejemplo.com/'

fetch(url)
    .then(response => {
        if(!response.ok){
            throw new Error('La solicitud fallo ')
        }
        return response.text()
    })
    .then(data => {
        console.log('Contenido del archivo: ', data)
    })
    .catch(error =>{
        console.error('Error', error)
    })









*/