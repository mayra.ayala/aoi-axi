// Importante instalar en nodo, npm install pdf-parse
const fs = require('fs')
const pdf = require('pdf-parse')

//El archivo PDF de ejemplo, se encuentra en la direccion colocada debajo
const archivoPDF = 'C:/Users/mayra_ayala/Documents/projecto-javascript/busqueda-de-texto-pdf/pdf/miarchivo.pdf'

//Leer un archivo PDF 
fs.readFile(archivoPDF, (err, data) => {
    if(err){
        console.error('Error al leer el archivo PDF: ', err)
        return
    }
// Utilizar pdf parse
     pdf(data).then(function (data){
        const textoExtraido = data.text
        console.log('texto extraido del PDF:',textoExtraido)
     })

})

