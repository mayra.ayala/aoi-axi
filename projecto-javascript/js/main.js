//Frontend

//Variables locales
let miboton    //Variable para boton
let contenido  //Variable que almacena el valor del input
let idprograma 
let componente
let defecto
let datatxt
let lineas
let posiciones // guarda lo que tiene la posicion x del array 
let etiqueta
let valor
let identifydefecto
let equivocado 
let estacionAOI
let estacionAXI
let bdalldata //variable que almacena los datos extraidos de la BD (un objeto)
let arraycomponentes = []
let allcomponents
let allelements 
let comp = [] //Guarda el componente en orden conforme el for lo vaya arrojando
let def = [] //Guarda el defecto
let equivocados = [] //array que guarda todos los elementos equivocados de la condicion
let fusionquivocados = [] // array donde se guardan las concatenaciones de defectos mas los equivocados 
let funcionllamda
let arrydiv = [] //Array alamacenara los datos falsos retornados de la BD
let arrycontenedor = []
let arryfromBD = [] //Array almacena los datos extraidos de BD 
let arrymerch = []
let arryTrue = [] //Guarda los datos que exiten en BD
let arryFalse = [] //Guarda los datos que no existen en BD
let arryagrupa = []
let nuevolog = [] //creara un nuevo log de falla
let array = [] //Guarda la informacion de "nuevolog"

let resultadodefuncion =  searchwordBD()
let nuevoarraydelog = datosnuevolog()


window.onload = async function(){
    return new Promise(async resolve => {
        //divhidden()
       /* const socket = io();
        socket.emit('AlldataBD')*/
        await gopen()
        //await deshabilitar()
    resolve('resolved')})}
//--------------------------------------- datos fecha ------------------------//
 //------------------------------- Muestra la hora local --------------------------------//
/*const d= new Date();
let hora = d.getHours(); //da el numero de la hora local ejemplo = 12, 13, 14
document.getElementById("demo").innerHTML = hora;*/
let hora = 1
//console.log(hora)
//------------------------------- Muestra dia de calendario -----------------------------//
const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
const dia = new Date();
let day = weekday[dia.getDay()];
//document.getElementById("demo").innerHTML = day;

//------------------------------- Muestra dia del mes en calendario -----------------------------//
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const m = new Date();
let month = months[m.getMonth()];
//document.getElementById("mes").innerHTML = month;

//------------------------------- Muestra el año  -----------------------------//
const numero = new Date()
let num = numero.getDate()
//document.getElementById("num").innerHTML = num

const año = new Date()
let anio = año.getFullYear()

const numdia = new Date()
let dias = numdia.getDay()

let fecha = anio+"-"+month+"-"+num
console.log(fecha)

//----------------------------------- Funcion de secuencia (solo si se requiere)-------------//
async function startsequence(){
    await inserta(contenido)
    //await goclose()
}
//------------------------------------funcion que guarda contenido de input en HTML ---------//
function valorinput(){ // funcion que guarda el elemento de input estacion
    return new Promise(async resolve =>{ 
    contenido = document.getElementById('myinput').value // Se almacena en una variable
    console.log(contenido)
    resolve('resolved')})
 }
 function valorinput2(){ // funcion que guarda el elemento de input idprograma
    return new Promise(async resolve =>{ 
        idprograma = document.getElementById('myinput2').value
        console.log(idprograma)
    resolve('resolved')})
 }
function valorinput3(){ // funcion que guarda el elemento de input componente
    return new Promise(async resolve =>{ 
    componente = document.getElementById('myinput3').value
    console.log(componente)
    resolve('resolved')})
}
function valorinput4(){ // funcion que guarda el elemento de input defecto
    return new Promise(async resolve =>{ 
    defecto = document.getElementById('myinput4').value
    console.log(defecto)
    resolve('resolved')})
}

//-funcion para agregar datos de los input a la BD
function btnagregar(){
    return new Promise(async resolve =>{ 
    miboton = document.getElementById('btninserta')
    await inserta(contenido,idprograma,componente,defecto,fecha) //Se manda llamar la funcion que trae el texto del input
    successful()
    await pause()
    setTimeout(function fire(){location.reload()})// temporizador para limpiar pantalla
    resolve('resolved')})
}

function divhidden(){
    document.getElementById('mydivform').style.visibility = "hidden"
}

//*********************************** Funciones con HTML ********************************* */
//Funcion para hacer visible en div de agregar
function SHagregar(){
    if(document.getElementById('mydivform').style.display = "none"){
        document.getElementById('mydivform').style.display = "block"
        console.log("estoy en if")
    }else {
        document.getElementById('mydivform').style.display = "none"
    }
}
//Funcion para hacer visible en div de consulta
function SHconsultar(){
    if(document.getElementById('formdos').style.display = "none"  ){
        document.getElementById('formdos').style.display = "block"
        valoreinput()
        console.log("EStoy en form consulta")
    }else{
        document.getElementById('formdos').style.display = "none"
    }

}

function SHacercade(){
    if(document.getElementById('acercad').style.display = "none"){
        document.getElementById('acercad').style.display = "block"
        console.log("Estoy en acercade")
    }else{
        document.getElementById('acercad').style.display = "none"
    }
}

 function resultados(){
    if(document.getElementById('resultados').style.display = "none"){
        document.getElementById('resultados').style.display = "block"
        console.log("Estoy en Resultados")
    }else{
        document.getElementById('resultados').style.display = "none"
    }
}

//----------------------------- Funciones debug ---------------------------------------
function newload(){
    setTimeout(function fire(){location.reload()})// temporizador para limpiar pantalla
}
async function pause() {
    return new Promise(async resolve => {
        setTimeout(function pausea() { resolve('resolved') }, 2000)
    });
}

//--------------------------------- Funcion principal para llamar a leer logs -----------------//
async function readfiles(){
    return new Promise(async resolve =>{
        const socket = io();
        socket.emit('readfiles')//Manda la bandera al backend para leer los datos de un TXT
        //socket.emit('AlldataBD')
        console.log("Consulta")
        successful1()
        resultados()
    resolve('resolved')})
}

//------------------------------------funciones para conexiones de cliente a BD ---------//

async function goclose(){
    return new Promise(async resolve =>{
    const socket = io();
    socket.emit('closeclient') 
    resolve('resolved')})
}
function experiment(){
    const socket = io();
    socket.emit('experiment') 
}
async function gopen(){
    return new Promise(async resolve =>{
    const socket = io();
    socket.emit('openclient') 
    setTimeout(function fire() { resolve('resolved'); }, 1000)})
}

//------------------------------------funciones para insertar datos ---------//
async function inserta(contenido,idprograma,componente,defecto,fecha){
    return new Promise(async resolve =>{
        let partNumber = "234MGC"
        console.log(contenido,idprograma,componente,defecto,fecha)
        const socket = io();
        socket.emit('insert',contenido,idprograma,componente,defecto,fecha) 
 
        resolve('resolved')})
}

//---------------------------- Respuesta de backend archivo ----------------//
const socket= io()
socket.on('datatext',function(data){
    datatxt =data
   // datalines(datatxt)
    //obtenerRestoDeLinea(datatxt)
    //iterararry()
    lineas = datatxt.split('\n')
    //console.log(lineas)
    searchingwrong(lineas)
    iterararry(lineas)
    
    //console.log("Extraccion final\n"+"Comp:"+comp+"\n"+"Fusion:"+ fusionquivocados)
})
 
/***************** BUSQUEDA DE DEFECTOS EN BD */
function searchingwrong(lineas){
    //var array=['SN=283005040267\r', 'Model=LFHR4045580_BOT\r', 'Shift=D\r', 'Rep_Date_Time=NA\r', 'OPID=Administrator\r', 'StationName=U/D\r', 'Surface=C\r', 'Result=Fail\r', 'Location=R6112-2\r', 'PinNum=0\r', 'ErrCode=COMPONENT_NG,Equivocado\r', 'RepairStatus=Repaired\r', 'RepairErrCode=N/A,N/A\r', 'Location=R6112-2\r', 'PinNum=0\r', 'ErrCode=MISSING,Faltante\r', 'RepairStatus=Repaired\r', 'RepairErrCode=N/A,N/A\r', 'Location=R6112-2\r', 'PinNum=0\r', 'ErrCode=MISSING,Faltante\r', 'RepairStatus=Repaired\r', 'RepairErrCode=N/A,N/A\r', 'Location=R6112-2\r', 'PinNum=0\r', 'ErrCode=LIFTED_BODY,Componente Elevado\r', 'RepairStatus=Repaired\r', 'RepairErrCode=N/A,N/A\r', 'Location=R6112-2\r', 'PinNum=0/r', 'ErrCode=SHIFT,Fuera de Registro\r', 'RepairStatus=Repaired\r', 'RepairErrCode=N/A,N/A\r', ''];
    var palabras = ["Location","Faltante","Equivocado","Componente Elevado","Fuera de Registro","Insuficiencia"]
    return lineas.filter(element =>{
        return palabras.some(palabra => element.includes(palabra))})
}

async function iterararry(lineas){
    return new Promise(async resolve => {
    var resultados = searchingwrong(lineas)
    
    for (var i = 0; i<resultados.length; i++){ 
    let results = resultados[i]
    console.log(results)
    let partes = results.split('=') //REaliza la separacion despues del = [Sn, 734794857]
    etiqueta = partes[0]
    valor = partes[1]
    var separarvalor = valor.split(',')
    identifydefecto = separarvalor[1]
    await valietiquetas(etiqueta,valor)
    }
    await exists(comp,fusionquivocados)
    socket.emit('AlldataBD')
    resolve('resolved')})
}

async function valietiquetas(etiqueta,valor){
    return new Promise(async resolve => {

    if (etiqueta == 'Location' ){
        var location = 'Location' //Guardamos unicamente el valor location del texto
        if(valor == 'COMPONENT_NG,Equivocado'){
            //console.log("No necesito este valor")
        }else{
            var x = valor
            //console.log("Soy x valor: ", x)
            comp.push(x)
            //console.log('Resultado de comp:',comp)
        }
    }
    await otra(etiqueta,valor)
    resolve('resolved')})
}
async function otra(etiqueta,valor){
    return new Promise(async resolve => {
    
    if(etiqueta=='ErrCode'){
        var errcode = 'Errcode'
        if(valor == 'COMPONENT_NG,Equivocado' ){
             equivocado = 'Equivocado'
            //console.log("DEFECTO:", equivocado)
            equivocados.push(equivocado)
            //console.log(equivocado)
        }else{
            var y = valor.split(',')
            defecto = y[1]
            //console.log("DEFECTO:", defecto)
            def.push(defecto)
            //console.log(def)
            }
        fusionquivocados =def.concat(equivocados) // Se concatenan elemento al final de array 'equivocado'
        }
     resolve('resolved');
    })
}


//************************************************** Busqueda de Locacion y defecto => "Equivocado" */
/*async function especific(lineas){
    console.log("Estoy en funcion especific...")
    var palabras2 = ["Model","Location","Equivocado"]
    return lineas.filter(element =>{
        return palabras2.some(palabra => element.includes(palabra))
    })
}

async function subespecific(lineas){
    var resultados2 = especific(lineas)
    for (var i = 0; i<resultados2.length; i++){
    let results2 = resultados2[i]
    console.log(resultados[i])
    console.log(results2)
    }
}*/
//--------------------------------------------------------------------------------------------------

//console.log(model())

/*function obtenerRestoDeLinea(datatxt) {

    let lineas = datatxt.split('\n')
    console.log("Soy lineas ",lineas)
    let palabra = "ErrCode"
    let posicionPalabra = lineas.indexOf(palabra)//busca la palabra en el texto 
    console.log("Soy posicion palabra",posicionPalabra)
    if (posicionPalabra !== -1) {
        let restoDeLinea = datatxt.substring(posicionPalabra)
        console.log("Soy resto de linea",restoDeLinea)
        document.getElementById('data').innerHTML= "Serial Number" + "<br>" + restoDeLinea
       // return restoDeLinea
    } else {
        return "Palabra no encontrada en la línea."
    }
}*/

// Ejemplo de uso
//var linea = "ErrCode=COMPONENT_NG,Equivocado"
//var resultado = obtenerRestoDeLinea(linea, palabra)
//console.log(resultado)

/*
function datalines(datatxt){
    lineas = datatxt.split('\n') //Convierte string en array 
    posiciones = lineas[1],lineas[2]//Pertenece a las posiciones del texto convertido en array
    console.log(posiciones)
    console.log("Soy primera linea del texto : ",lineas[1])
    for(var linea of lineas){
        console.log(linea)
    }
    searchwords(datatxt,posiciones)//Funcion a la que se le pasan los parametros para buscar en el texto 
    //Se pasa como argumento el texto completo y la palabra a buscar
}*/

/*async function searchwords(datatxt,posiciones){
    return new Promise(async resolve =>{
       let palabra = 'SN' //Ejemplo de palabra a leer 
       if (datatxt.indexOf(posiciones) !== -1){
           console.log('Encontre esa palabra en el archivo', posiciones)
           document.getElementById('data').innerHTML= "Serial Number" + "<br>" + posiciones
           }else{
               console.log('No encontre esa palabra', datatxt)
           }
    resolve('resolved')}) 
}*/

//------------------------------Funciones para desactivar y activar boton------------------
function deshabilitar(){
    return new Promise(async resolve =>{
    var btndisabled = document.getElementById('btnconsultaAOI')
    btndisabled.disabled = true
    console.log("Estoy deshabilitado")
    resolve('resolved')}) 
}
function habilitar(){
    return new Promise(async resolve =>{
    var btndisabled = document.getElementById('btnconsultaAOI')
    btndisabled.disabled = false
    console.log("Estoy habilitado")
    resolve('resolved')}) 
}

//Boton AOI 
//Funcion que manda llamar si el boton esta habilitado/deshabilitado y leer log de falla
async function valoreinput(){
    return new Promise(async resolve =>{
    var select = document.getElementById('optionals')
    var value = select.options[select.selectedIndex].value
    
    var selects = document.getElementById('optionals2')
    var values = selects.options[selects.selectedIndex].value
    if(value === " "){
         //deshabilitar()
         console.log("Seleccione linea")
         warning()
    }else if((value === "1") || (value === "2" ) || (value === "3" ) || (value === "4" ) && (values === "1") ){
         habilitar()
        readfiles()
        console.log("Linea W")
    }else if((value === "1") ){ //&& (values === "2")
        habilitar()
        readfiles()
        console.log("Linea x")
    }else if((value === "2") && (values === "3")){
        habilitar()
        readfiles()
        console.log("Linea y")
    }else if((value === "3") && (values === "4")){
         habilitar()
         readfiles()
        console.log("Linea z")
    }else if (values === "4"){
         habilitar()
         readfiles()
    }
    //console.log(value)
    resolve('resolved')}) 
}
//Boton AXI 
//Funcion que manda llamar si el boton esta habilitado/deshabilitado y leer log de falla
async function valoreinputAXI(){
    return new Promise(async resolve =>{
        var selection = document.getElementById('options')
        var value = selection.options[selection.selectedIndex].value
        if(value === " "){
             //deshabilitar()
             console.log("Seleccione linea")
             warning()
        }else if(value === "1"){
             habilitar()
            readfiles()
            console.log("Linea W")
        }else if(value === "2"){
            habilitar()
            readfiles()
            console.log("Linea x")
        }else if(value === "3"){
            habilitar()
            readfiles()
            console.log("Linea y")
        }else if(value === "4"){
             habilitar()
            readfiles()
            console.log("Linea z")
        }
        console.log(value)
        resolve('resolved')}) 
}

/********************************************* Funcion PAUSE */
async function pauses() {
    return new Promise(async resolve => {
        console.log('Espere 3 segundos...')
        setTimeout(function pausea() { resolve('resolved') }, 2000)
    });}

/***************************************************** Consulta de Falsos **************************************************/
//Logica de respuesta => Si sale en el log de falla y no esta registrado en la BD == 'falso'
//Si esta registrado en la BD pero no esta en el log == 'Escape'

async function exists(comp,fusionquivocados){
   return new Promise(async resolve => {
    //Limpieza de arrays (\r)
    const cleanArray = comp.map(element => element.replace(/\r/g,''));
    //console.log(cleanArray)
    const cleanArray2 = fusionquivocados.map(element => element.replace(/\r/g,''));
    //console.log(cleanArray2)
    await consult(cleanArray,cleanArray2)
    resolve('resolved');})}   

async function consult(cleanArray,cleanArray2){
    return new Promise(async resolve => {
    let n = cleanArray.length
    for(let j = 0; j<n; j++){ //comp.length
        let elementoarray1 = cleanArray[j]
        let elementoarray2 = cleanArray2[j]
        //console.log('Elemento 1: '+ elementoarray1)
        //console.log('Elemento 2: '+ elementoarray2)
        //console.log(allelements)
        //await elementslog(elementoarray1,elementoarray2)
        await consultaAOI(elementoarray1,elementoarray2)
        //console.log('final de iteracion',j)
        allelements = elementoarray1 +"-"+elementoarray2
        //console.log(allelements)
        nuevolog.push(allelements)
        resolve('resolved')}
        //ELEMENTOS QUE SE GUARDAN UNA VEZ QUE HAYA TERMINADO DE ITERAR EL FOR
        elementslog(nuevolog)
    }
)}

//Funcion que manda los datos requeridos ya procesados
async function consultaAOI(elementoarray1,elementoarray2){
    return new Promise(async resolve => {
        estacionAOI = "AOI"
        //console.log(elementoarray1,elementoarray2)
        const socket = io();
        socket.emit('exist',estacionAOI, elementoarray1,elementoarray2)//Estacion. componente, defecto
        //console.log("Datos a mandar a backend: ",estacionAOI, elementoarray1,elementoarray2) 
        let newfusion = estacionAOI + "," + elementoarray1 +","+ elementoarray2
        arrymerch =  newfusion.split(",")
        //console.log("esta es la fusion: ",arrymerch)
        resolve('resolved');})}

//Funcion que manda los datos requeridos ya procesados
async function consultaAXI(elementoarray1,elementoarray2){
    return new Promise(async resolve => {
        //setTimeout(() => {
        estacionAOI = "AXI"
        console.log(elementoarray1,elementoarray2)
        const socket = io();
        socket.emit('exist',estacionAOI, elementoarray1,elementoarray2) 
    resolve('resolved');
        //},1000);
    })
}

/*********************************************** Respuesta de BD  */
socket.on('bdresponse', function (resuts) {

let returnComponente = resuts.componente //Muestra en consola el componente que retorno la BD
let returnDefecto = resuts.defecto //Muestra el defceto que retorno la BD

let datosBD = resuts.result.rows[0]
let resultado = datosBD.exists
//console.log(resultado + returnComponente + returnDefecto )

//*******************Muestra de datos retornados de BD */
let dataCD = resultado +"-"+returnComponente + '-' + returnDefecto 
arrydiv.push(dataCD)
if (resultado === true){
    arryTrue.push(resultado +"-"+returnComponente + '-' + returnDefecto)
    //console.log("Respuesta = si existe en BD: ",arryTrue)
}else if(resultado === false){
    arryFalse.push(resultado +"-"+returnComponente + '-' + returnDefecto)
    //console.log("Respuesta = No existe en BD => falso",arryFalse)
    document.getElementById('data').innerHTML= "Respuesta: " + "<br>" + "No hay registro en BD => FALSO:" + "<br>" +' ' + arryFalse
    //console.log(arrydiv)
}
})


/******************************************* BUSQUEDA DE LA BD AL LOG */
/************************ Funciones para buscar si existen datos de BD en el log */
function elementslog(nuevolog){
   //ELEMENTOS DE LOG DE FALLA COMPONENTE DEFECTO
     array = nuevolog
     datosnuevolog(array)
}

/**************************** Resultados consulta de todos los datos en BD  */
socket.on('dataBD',function(data){
    bdalldata = data
    console.log(bdalldata)
    //searchbd(bdalldata)
    iterows(bdalldata)
})

//FUNCION IMPRIME TODOS LOS ARRAYS ENCONTRADOS EN ROWS 
//ROWS se extrae de la base de datos GOLDEN
async function iterows(bdalldata){
   // console.log("Este es arrymerch en iterows: ",arrymerch)
    let conteiner = bdalldata.rows // Direccionamiento de array de objetos alojado en una variable
    //console.log("Este es el conteiner ",conteiner)
    for(let k = 0; k <conteiner.length; k++){ //itera los arrays encontrados en rows
        let alldata = conteiner[k]
        //console.log(alldata)
        var stat = alldata.estacion
        var com  = alldata.componente
        var def  = alldata.defecto
        let agrupa = com+"-"+def
        arryagrupa = agrupa.split(",")
        let agrupadata = stat+ "-"+com +"-"+ def
        arryfromBD = agrupadata.split(",")
        //console.log("This is agrupadata from BD: ",arryfromBD)
         searchwordBD(arryagrupa)//Mandara las palabras a buscar de la BD al LOG uno por uno
    }
}

/************** FUNCION QUE BUSCA EN EL LOG DE FALLA LOS DATOS DE LA BD */
function searchwordBD(arryagrupa){
    console.log(arryagrupa)
    let buscar = 0
    //Se guarda en una variable el array que retorna la funcion 
    //El array de la funcion trae los datos encontrados del log de falla
    let logdata = datosnuevolog(array)
    console.log(logdata)
    //La variable "buscar" guarda el dato si coincide la busqueda de los elementos de la BD con el log de falla
    buscar = logdata.some((element) => element == arryagrupa)
    /**NOTA 
     * Si esta registrado en la base de datos pero no esta en el log entonces resultado "ESCAPE"
     * 
    */
    if(buscar == true){
        console.log("REGISTRADO")
    }else if(buscar == false){
        console.log("ESCAPE ")
        document.getElementById('data').innerHTML="Respuesta: " +"<br>" + "<br>"+ "No se encontro en log de falla" + "<br>" + '' + buscar
    }
}

//fUNCION PARA RETORNAR LOS ELEMENTOS DEL NUEVO LOG
function datosnuevolog(array){
    return array
}

/************************ CODIGO DE EJEMPLO */
//EJEMPLO COMPARACION DE VARIABLES DE DIFERENTES FUNCIONES
let x = 4
function res(){
    let resultadoA = A()
    let resultadoB = B()

    function A(x){
        
        return ;
    }
    
    function B(){
        let y = 4
        return ;
    }
    if(resultadoA === resultadoB){
        console.log("valores iguales")
    }else{
        console.log("Valores diferentes")
    }
}





